package com.uca.dao;

import com.uca.entity.CellEntity;
import com.uca.entity.GridEntity;
import org.postgresql.replication.PGReplicationConnection;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GrilleDAO {

    public List<CellEntity> getGrid(Integer id) throws SQLException {

        Connection connection = _Connector.getConnection(id);
        List<CellEntity> grid = new ArrayList<>();

        try{

            PreparedStatement request = connection.prepareStatement(
                    "SELECT indiceX,indiceY,etat FROM grille;"
            );
            ResultSet results = request.executeQuery();
            while(results.next())
            {
                Integer x;
                Integer y;
                boolean status;

                x = results.getInt("indiceX");
                y = results.getInt("indiceY");
                status = results.getBoolean("etat");
                if(status) {
                    CellEntity cell = new CellEntity(x, y);
                    cell.setStatus(status);
                    grid.add(cell);
                }
            }
            return grid;

        }catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public boolean empty(List<CellEntity> grid, Integer id) throws SQLException {

        Connection connection = _Connector.getConnection(id);

        try {

            for (int i = 0; i < grid.size(); i++)
            {

                PreparedStatement request = connection.prepareStatement(
                        "UPDATE grille SET etat = 'false' WHERE indiceX = ? AND indiceY = ?;"
                );
                request.setInt(1,grid.get(i).getX());
                request.setInt(2,grid.get(i).getY());
                request.executeUpdate();

            }

            return true;

        }catch (SQLException e){
            e.printStackTrace();
            connection.rollback();
            return false;

        }
    }

    public boolean next(Integer xmin, Integer xmax, Integer ymin, Integer ymax,Integer id) throws SQLException {

        Connection connection = _Connector.getConnection(id);

        try{

            for (int i = xmin - 1; i <= xmax + 1; i++)
            {
                for (int j = ymin - 1; j <= ymax + 1; j++)
                {
                    PreparedStatement request = connection.prepareStatement(
                            "SELECT voisinVivants,etat FROM grille WHERE indiceX = ? AND indiceY = ?;"
                    );
                    request.setInt(1,i);
                    request.setInt(2,j);
                    ResultSet result = request.executeQuery();
                    
                    if(result.next())
                    {
                        System.out.println(result.getInt("voisinVivants"));
                        System.out.println("changement en false" + i + j);

                        if(result.getInt("voisinVivants") == 3 || (result.getBoolean("etat") && result.getInt("voisinVivants") == 2))
                        {
                            PreparedStatement change = connection.prepareStatement(
                                    "UPDATE grille SET etat = 'true' WHERE indiceX = ? AND indiceY = ?;"
                            );
                            change.setInt(1,i);
                            change.setInt(2,j);
                            change.executeUpdate();
                        }
                        else
                        {
                            System.out.println(result.getInt("voisinVivants"));
                            System.out.println("changement en false" + i + j);
                            PreparedStatement change = connection.prepareStatement(
                                    "UPDATE grille SET etat = 'false' WHERE indiceX = ? AND indiceY = ?;"
                            );
                            change.setInt(1,i);
                            change.setInt(2,j);
                            change.executeUpdate();
                        }
                    }
                }
            }
            if(setVoisin(xmin,xmax,ymin,ymax,connection))
            {
                return true;
            }

            connection.rollback();
            return false;

        }catch (Exception e){
            e.printStackTrace();
            connection.rollback();
            return false;
        }

    }

    public boolean setVoisin(Integer xmin, Integer xmax,Integer ymin, Integer ymax, Connection connection) {

        try {

            for (int i = xmin - 2; i < xmax + 2; i++) {

                for (int j = ymin - 2; j < ymax + 2; j++) {

                    Integer nbVoisins = 0;

                    PreparedStatement request = connection.prepareStatement(
                            "SELECT etat FROM grille WHERE indiceX = ? AND indiceY = ?;"
                    );
                    request.setInt(1, i -1);
                    request.setInt(2, j -1);
                    ResultSet result = request.executeQuery();
                    if (result.next()) {
                        if (result.getBoolean("etat")) {
                            nbVoisins ++;
                        }
                    }

                    request = connection.prepareStatement(
                            "SELECT etat FROM grille WHERE indiceX = ? AND indiceY = ?;"
                    );
                    request.setInt(1, i);
                    request.setInt(2, j -1);
                    result = request.executeQuery();
                    if (result.next()) {
                        if (result.getBoolean("etat")) {
                            nbVoisins ++;
                        }
                    }

                    request = connection.prepareStatement(
                            "SELECT etat FROM grille WHERE indiceX = ? AND indiceY = ?;"
                    );
                    request.setInt(1, i + 1);
                    request.setInt(2, j -1);
                    result = request.executeQuery();
                    if (result.next()) {
                        if (result.getBoolean("etat")) {
                            nbVoisins ++;
                        }
                    }

                    request = connection.prepareStatement(
                            "SELECT etat FROM grille WHERE indiceX = ? AND indiceY = ?;"
                    );
                    request.setInt(1, i - 1);
                    request.setInt(2, j);
                    result = request.executeQuery();
                    if (result.next()) {
                        if (result.getBoolean("etat")) {
                            nbVoisins ++;
                        }
                    }

                    request = connection.prepareStatement(
                            "SELECT etat FROM grille WHERE indiceX = ? AND indiceY = ?;"
                    );
                    request.setInt(1, i + 1);
                    request.setInt(2, j);
                    result = request.executeQuery();
                    if (result.next()) {
                        if (result.getBoolean("etat")) {
                            nbVoisins ++;
                        }
                    }

                    request = connection.prepareStatement(
                            "SELECT etat FROM grille WHERE indiceX = ? AND indiceY = ?;"
                    );
                    request.setInt(1, i - 1);
                    request.setInt(2, j + 1);
                    result = request.executeQuery();
                    if (result.next()) {
                        if (result.getBoolean("etat")) {
                            nbVoisins ++;
                        }
                    }

                    request = connection.prepareStatement(
                            "SELECT etat FROM grille WHERE indiceX = ? AND indiceY = ?;"
                    );
                    request.setInt(1, i);
                    request.setInt(2, j + 1);
                    result = request.executeQuery();
                    if (result.next()) {
                        if (result.getBoolean("etat")) {
                            nbVoisins ++;
                        }
                    }

                    request = connection.prepareStatement(
                            "SELECT etat FROM grille WHERE indiceX = ? AND indiceY = ?;"
                    );
                    request.setInt(1, i + 1);
                    request.setInt(2, j + 1);
                    result = request.executeQuery();
                    if (result.next()) {
                        if (result.getBoolean("etat")) {
                            nbVoisins ++;
                        }
                    }

                    System.out.println("Nombre de voisins vivants = " + nbVoisins);
                    request = connection.prepareStatement(
                            "UPDATE grille SET voisinVivants = ?;"
                    );
                    request.setInt(1,nbVoisins);
                    request.executeUpdate();


                }
            }
            return true;
        } catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
    }
}
