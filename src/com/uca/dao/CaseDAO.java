package com.uca.dao;

import com.uca.entity.CellEntity;
import java.sql.*;
public class CaseDAO {

    public static void change(CellEntity cell, Integer id) throws SQLException {
        Connection connection = _Connector.getConnection(id);
        try {

            PreparedStatement request = connection.prepareStatement("SELECT etat FROM grille where indiceX = ? and indiceY = ?;");
            request.setInt(1,cell.getX());
            request.setInt(2,cell.getY());
            ResultSet result = request.executeQuery();
            result.next();
            boolean value = result.getBoolean("etat");
            PreparedStatement change;
            if(value) {
                change = connection.prepareStatement("UPDATE grille SET etat = 'false' WHERE indiceX = ? and indiceY = ?;");
            }else{
                change = connection.prepareStatement("UPDATE grille SET etat = 'true' WHERE indiceX = ? and indiceY = ?;");
            }
            change.setInt(1,cell.getX());
            change.setInt(2,cell.getY());
            change.executeUpdate();
            new  GrilleDAO().setVoisin(cell.getX(), cell.getX(), cell.getY(), cell.getY(), connection);
        }catch (SQLException e)
        {
            e.printStackTrace();
            connection.rollback();
        }
    }

}
