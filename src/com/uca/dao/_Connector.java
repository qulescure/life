package com.uca.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

public class _Connector {

    private static String url = "jdbc:postgresql://localhost:5432/postgres";
    private static String user = "test";
    private static String passwd = "test";

    private static Connection connect;
    private static HashMap<Integer,Connection> allConnexion = new HashMap<>();

    public static Connection getConnection(Integer id){

        if(allConnexion.containsKey(id)){
            return  allConnexion.get(id);
        }
        return getNewConnection(id);
    }

    private static Connection getNewConnection(Integer id) {
        Connection c ;
        try {
            c = DriverManager.getConnection(url, user, passwd);
            c.setAutoCommit(false);
            c.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
        } catch (SQLException e) {
            System.err.println("Erreur en ouvrant une nouvelle connection.");
            throw new RuntimeException(e);
        }
        allConnexion.put(id,c);
        return c;
    }

    public static Connection getMainConnexion() throws SQLException {

        if(connect == null)
        {
            connect = DriverManager.getConnection(url, user, passwd);
        }

        return connect;
    }
}
