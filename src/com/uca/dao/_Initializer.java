package com.uca.dao;

import org.postgresql.replication.PGReplicationConnection;

import java.sql.*;

public class _Initializer {
    // nom de la table contenant la grille
    final static String TABLE = "grille";
    // taille de grille
    final static int SIZE = 10000;

    /**
     * cette méthode permet d'initialiser en créant une table pour la grille si elle n'existe pas
     */
    public static void Init() throws SQLException {
        Connection connexion =_Connector.getMainConnexion();

        if (!tableExists(connexion, TABLE)){
            try {
                PreparedStatement request = connexion.prepareStatement("CREATE TABLE grille (indiceX int, indiceY int,etat boolean,voisinVivants int, PRIMARY KEY (indiceX,indiceY));");
                request.executeUpdate();
                for(int i = 0; i < 100; i++)
                {
                    for (int j = 0; j < 100; j++) {
                        PreparedStatement add = connexion.prepareStatement("INSERT INTO grille VALUES (?,?,'false',0);");
                        add.setInt(1,i);
                        add.setInt(2,j);
                        add.executeUpdate();
                    }
                }
            }catch (SQLException e){
                    e.printStackTrace();
            }
        }
    }

    /**
     * teste si une table existe dans la base de données 
     */
    private static boolean tableExists(Connection connection, String tableName) throws SQLException {
        DatabaseMetaData meta = connection.getMetaData();
        ResultSet resultSet = meta.getTables(null, null, tableName, null);
        return resultSet.next();
    }
}
