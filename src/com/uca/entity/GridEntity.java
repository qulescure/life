package com.uca.entity;

import com.uca.core.GridCore;

import java.net.Inet4Address;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

/**
 * Représente l'état de la grille
 * Toutes les cellules de l'état de la grille sont vivantes
 */
public class GridEntity {
    // état de la grille : liste des cellules vivantes
    private final List<CellEntity> state;

    public GridEntity() {
        state = new ArrayList<>();
    }

    /**
     * retourne la grille
     */
    public List<CellEntity> getCells(Integer id) throws SQLException {

        List<CellEntity> grid = GridCore.getAllGrid(id);
        for (int i = 0; i < grid.size(); i++)
        {
            CellEntity cell = grid.get(i);
            this.addCell(cell);
        }
        return state;
    }

    /**
     * ajoute une cellule vivante à la grille 
     */
    public void addCell(CellEntity c) {
        state.add(c);
    }

    public boolean next(Integer id) throws SQLException {
        CellEntity cell = state.get(0);
        Integer xmin = cell.getX();
        Integer xmax = cell.getX();
        Integer ymin = cell.getY();
        Integer ymax = cell.getY();

        for (int i = 1; i < state.size(); i++)
        {
            cell = state.get(i);
            if(xmin > cell.getX())
            {
                xmin = cell.getX();
            }
            else if(xmax < cell.getX())
            {
                xmax = cell.getX();
            }

            if(ymin > cell.getY())
            {
                ymin = cell.getX();
            }
            else if(ymax < cell.getY())
            {
                ymax = cell.getY();
            }
        }

        return GridCore.next(xmin, xmax,ymin,ymax,id);

    }

    public void save(GridEntity grid) {
        GridEntity gridSave = new GridEntity();
        for (int i = 0; i < grid.state.size(); i++) {
            gridSave.state.add(grid.state.get(i));
        }

    }
}
