# Projet Life Lescure-Blanchon



## Présentation du projet
Voici notre projet de base de données. Nous formons un binôme.

## Etapes principales
- mise en place de la connexion a la base de données et installation de postgresql sur nos machines personnelles.
- jonction entre la base de données et java
- réponse aux premieres questions : configuration de notre espace de travail ainsi que initialisation de la base de données.
- possibilité de changer l'état d'une cellule
- chargement d'une grille via le DAO
- possibilité de changer une case de la grille
- implémentation des boutons de l'interface (next, save...)

## Difficultés rencontrées
La mise en place du code pour le rafraichissement de la page à été plus longue que prévu.
Création de GrilleDAO afin de recupérer la grille en base de données pour la ré-afficher sur la page ftl.
Problèmes d'initialisation de la grille qui ont été réglés plus tard.